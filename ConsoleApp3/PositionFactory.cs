﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToeProject
{
    class PositionFactory
    {
        public PositionFactory()
        {

        }

        public BoardPosition CreatePosition(PositionState state, int index, int[] coords)
        {
            BoardPosition bp = new BoardPosition(index, state, coords);
            return bp;
        }
    }
}
