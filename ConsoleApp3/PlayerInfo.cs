﻿using System;

namespace TicTacToeProject
{
    public class PlayerInfo
    {
        public char playerChar;
        public PositionState playerState;
        public string playerName;

        static private char defaultChar = 'B';
        static private PositionState defaultState = PositionState.Empty;
        static private string defaultName = "Kuiper";


        //default constructor (no arguments)
        //useful if we want to create the object but aren't quite ready to commit to details
        public PlayerInfo()
        {
            this.playerChar = defaultChar;
            this.playerState = defaultState;
            this.playerName = defaultName;
        }

        //3-argument constructor
        public PlayerInfo(char pc, PositionState ps, string pname)
        {
            this.playerChar = pc;
            this.playerState = ps;
            this.playerName = pname;
        }

    }
}