﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToeProject
{

    public enum Divider {  Horizontal, Vertical };

    class BoardRenderer
    {
        static private string horizDivider = "----+-----+----";
        static private string vertDivider = " | ";
        private char charToRender;

        public BoardRenderer()
        { 
        }

        public void RenderRow(BoardPosition[] _positionRow)
        {
            for(int i = 0; i < _positionRow.Length; i++)
            {
                RenderSymbol(_positionRow[i]);

                if(i != (_positionRow.Length - 1))
                {
                    RenderDivider(Divider.Vertical);
                }
            }

            Console.WriteLine();
        }

        public void RenderSymbol(BoardPosition currentPos)
        {
            PositionState state = currentPos.state;
            int _index = currentPos.index;
            char indexChar = (char)_index;

            if(state == PositionState.X)
            {
                charToRender = 'X';
                Console.Write(" " + charToRender + " ");
            }
            else if(state == PositionState.O)
            {
                charToRender = 'O';
                Console.Write(" " + charToRender + " ");
            }
            else if(state == PositionState.Empty)
            {
                Console.Write(" " + currentPos.index + " ");
            }
        }

        public void RenderDivider(Divider d)
        {
            if(d == Divider.Horizontal)
            {
                Console.Write(horizDivider);
            }
            else if(d == Divider.Vertical)
            {
                Console.Write(vertDivider);
            }
        }
    }
}
