﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToeProject
{
    class SessionInfo
    {
        public PositionState pwin;
        public GameState endState;

        public SessionInfo(PositionState _pwin, GameState _endState)
        {
            this.pwin = _pwin;
            this.endState = _endState;
        }
    }
}
