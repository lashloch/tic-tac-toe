﻿using System;

namespace TicTacToeProject
{
    class Board
    {
        public BoardPosition[][] positions;
        public int boardSize = 3;

        private BoardRenderer br;
        private BoardChecker bc;

        public Board()
        {
            this.positions = new BoardPosition[boardSize] [];

            //this is cheap, but will work for now
            positions[0] = new BoardPosition[boardSize];
            positions[1] = new BoardPosition[boardSize];
            positions[2] = new BoardPosition[boardSize];

            br = new BoardRenderer();
            bc = new BoardChecker();
        }



        public void ChangeBoard(BoardPosition bp)
        {
            int x, y;
            int[] _coords = bp.coords;

            x = _coords[0];
            y = _coords[1];

            positions[x][y] = bp;
        }

        public bool CheckBoardForLegalMove(BoardPosition bp)
        {
            bool isLegal = bc.CheckSpace(bp);
            return isLegal;
        }

        public SessionInfo CheckBoardForGameOver()
        {
            SessionInfo ss = bc.CheckEnd(positions);
            return ss;
        }

        public void PrintBoard()
        {
            for(int i = 0; i < boardSize; i++)
            {
                br.RenderRow(positions[i]);
                if(i != (boardSize - 1))
                {
                    br.RenderDivider(Divider.Horizontal);
                    Console.WriteLine();
                }
            }
            Console.WriteLine();
        }

        public BoardPosition PositionFromIndex(int index)
        {
            BoardPosition bp = new BoardPosition(69, PositionState.Empty, new int[2] { 666, 666 });
            for(int i = 0; i < boardSize; i++)
            {
                for(int j = 0; j < boardSize; j++)
                {
                    if(positions[i][j].index == index)
                    {
                        bp = positions[i][j];
                        return bp;
                    }
                }
            }
            return bp;
        }
    }
}
