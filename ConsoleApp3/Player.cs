﻿using System;

namespace TicTacToeProject
{
    public class Player
    {
        public PlayerInfo playerInfo;

        static public string playerName1 = "Player 1";
        static public string playerName2 = "Player 2";

        //default constructor
        public Player()
        { 
            this.playerInfo = new PlayerInfo();
        }

        public Player(char c, string playerName)
        {
            PlayerInit(c, playerName);
        }        
        
        public void PlayerInit(char c, string playerName)
        {
            switch(c)
            {
                case 'X':
                    this.playerInfo = new PlayerInfo('X', PositionState.X, playerName);
                    break;
                case 'O':
                    this.playerInfo = new PlayerInfo('O', PositionState.O, playerName);
                    break;
                default:
                    break;
            }

      
        }
    }
}
