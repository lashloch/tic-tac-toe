﻿using System;

namespace TicTacToeProject
{
    class GameInput
    {

        public GameInput()
        {
        }

        public void WaitForAnyKey()
        {
            Console.ReadKey(true); //don't show the key pressed by the user in the console
        }

        public char GetKey()
        {
            char c = Console.ReadKey(true).KeyChar;
            c= Char.ToUpper(c);
            return c;
        }

        public bool AskYesOrNo()
        {
            bool yn;
            char c;

            c = GetKey();
            while (c != 'Y' && c != 'N')
            {
                c = GetKey();
            }

            if (c == 'Y')
            {
                yn = true;
            }
            else yn = false;

            return yn;
        }
        public char GetIndex()
        {
            char c;
            c = Console.ReadKey(true).KeyChar;

            while (c != '1' && c != '2' && c != '3' && c != '4' && c != '5' && c != '6' && c != '7' && c != '8' && c != '9')
            {
                c = Console.ReadKey(true).KeyChar;
            }
            return c;
        }
        public void Intro()
        {
            Console.WriteLine("Weclome to Tic Tac Toe!");
            Console.WriteLine();

            Console.WriteLine("Instructions: ");
            Console.WriteLine("  Before starting the game, decide who is Player 1 and and who is Player 2.");
            Console.WriteLine();

            Console.WriteLine("  When ready, press any key to acknowledge messages like this one.");
            Console.ReadKey(true);
            Console.WriteLine();

            Console.WriteLine("Let's get started!");
            Console.ReadKey(true);
            Console.WriteLine();

            Console.Clear();
        }
    }
}
