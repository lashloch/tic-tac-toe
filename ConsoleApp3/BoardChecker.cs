﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToeProject
{
    public enum GameState { NotOver, Win, Draw };
    
    class BoardChecker
    {
        private GameState _gameState;
        private PositionState pwinner;

        public BoardChecker()
        {
            this._gameState = GameState.NotOver;
            this.pwinner = PositionState.Empty;
        }

        public bool CheckSpace(BoardPosition bp)
        {
            bool empty = true;

            if(bp.state != PositionState.Empty)
            {
                empty = false;
            }

            return empty;
        }

        public SessionInfo CheckEnd(BoardPosition[][] bp)
        {
            SessionInfo si;

            CheckWinOrDraw(bp);
            si = new SessionInfo(pwinner, _gameState);
            return si;
        }

        public void CheckWinOrDraw(BoardPosition[][] bp)
        {
            bool winFound = false;
            bool drawFound = true;

            //check rows
            for(int i = 0; i < bp.GetLength(0); i++)
            {
                if (bp[i][0].state != PositionState.Empty && bp[i][0].state == bp[i][1].state && bp[i][1].state == bp[i][2].state)
                {
                    winFound = true;
                    _gameState = GameState.Win;
                    pwinner = bp[i][0].state;
                }
            }

            if (!winFound)
            {
                //check columns
                for (int i = 0; i < bp.GetLength(0); i++)
                {
                    if (bp[0][i].state != PositionState.Empty && bp[0][i].state == bp[1][i].state && bp[1][i].state == bp[2][i].state)
                    {
                        winFound = true;
                        _gameState = GameState.Win;
                        pwinner = bp[0][i].state;
                    }
                }
            }

            if (!winFound)
            {
                //check diagonals
                if (bp[0][0].state != PositionState.Empty && bp[0][0].state == bp[1][1].state && bp[1][1].state == bp[2][2].state)
                {
                    winFound = true;
                    _gameState = GameState.Win;
                    pwinner = bp[0][0].state;
                }
                else if (bp[2][0].state != PositionState.Empty && bp[2][0].state == bp[1][1].state && bp[1][1].state == bp[0][2].state)
                {
                    winFound = true;
                    _gameState = GameState.Win;
                    pwinner = bp[2][0].state;
                }
            }

            if (!winFound)
            {
                //check for draw
                for (int i = 0; i < bp.GetLength(0); i++)
                {
                    for (int j = 0; j < bp.GetLength(0); j++)
                    {
                        if (bp[i][j].state == PositionState.Empty)
                        {
                            _gameState = GameState.NotOver;
                            drawFound = false;
                            break;
                        }
                    }
                }
            }

            if (drawFound && !winFound)
            {
                _gameState = GameState.Draw; //set state to Draw
            }
        }
    }
}


