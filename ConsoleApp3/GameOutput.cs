﻿using System;


namespace TicTacToeProject
{
    class GameOutput
    {
        private static string hrule = "-------------------";
        public void PromptAnyKey()
        {
            Console.WriteLine("(Press any key to continue)");
            Console.WriteLine();
        }
        public void DoIntro()
        {
            Console.WriteLine("VIRTUAL TIC-TAC-TOE");
            Console.WriteLine(hrule);
            Console.WriteLine();
            Console.WriteLine("IMPORTANT: Decide amongst yourselves who is Player 1 and who is Player 2.");
            Console.WriteLine();
        }

        public void DoLetterChoice()
        {

            Console.WriteLine("Player 1: press either the X or O key to play as that letter.");
            Console.WriteLine();
        }

        public void AnnounceLetters(Player p1, Player p2)
        {
            Console.Clear();
            Console.WriteLine("LETTERS");
            Console.WriteLine(hrule);
            Console.WriteLine(p1.playerInfo.playerName + ": " + p1.playerInfo.playerChar);
            Console.WriteLine(p2.playerInfo.playerName + ": " + p2.playerInfo.playerChar);
            Console.WriteLine();
        }

        public void AnnounceFirstPlayer(Player pfirst)
        {
            Console.WriteLine("FIRST UP");
            Console.WriteLine(hrule);
            Console.WriteLine(pfirst.playerInfo.playerName + " (" + pfirst.playerInfo.playerChar + ")");
            Console.WriteLine();
        }

        public void DoInstructions()
        {
            Console.WriteLine("Press a number key 1-9 to make your move.");
        }
        public void AnnounceTurn(Player currentPlayer)
        {
            Console.WriteLine("It is " + currentPlayer.playerInfo.playerName + " (" + currentPlayer.playerInfo.playerChar + ")'s turn!");
            Console.WriteLine();
        }

        public void AnnounceWin(Player pwin)
        {
            Console.WriteLine(pwin.playerInfo.playerName + " (" + pwin.playerInfo.playerChar + ") wins!");
            Console.WriteLine();
        }

        public void AnnounceDraw()
        {
            Console.WriteLine("The game is a draw!");
            Console.WriteLine();
        }

        public void AskToPlayAgain()
        {
            Console.WriteLine("Want to play again? (Press Y/N)");
            Console.WriteLine();
        }

        public void SayGoodbye()
        {
            Console.WriteLine(hrule);
            Console.WriteLine("Thanks for playing!");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("(Press any key to exit.)");
        }
    }
}
