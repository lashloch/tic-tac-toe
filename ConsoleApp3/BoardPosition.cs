﻿using System;

namespace TicTacToeProject
{

    
    public enum PositionState { Empty, X, O };

    public class BoardPosition
    {
        public int index;
        public PositionState state;
        public int[] coords;

        public BoardPosition(int index, PositionState state, int[] coords)
        {
            this.index = index;
            this.state = state;
            this.coords = coords;
        }

    }
}
