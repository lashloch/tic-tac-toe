﻿using System;

namespace TicTacToeProject
{
    public class Game
    {

        public static void Main()
        {
            Session session;
            bool playAgain;
            bool firstGame;

            session = new Session(true);
            playAgain = session.PlayOneGame();

            while(playAgain)
            { 
                session = new Session(false);
                playAgain = session.PlayOneGame();
                //sanity check
            }
        }
    }
}
