﻿using System;


namespace TicTacToeProject
{
    class Session
    {
        public Player p1, p2;
        public Player[] players;
        public Board board;
        public bool firstSession;

        private BoardFactory bf;
        private GameInput gi;
        private GameOutput go;
        
        //constructor
        public Session(bool _firstGame)
        {
            this.firstSession = _firstGame;
            this.bf = new BoardFactory();

            this.p1 = new Player();
            this.p2 = new Player();

            this.gi = new GameInput();
            this.go = new GameOutput();
            this.players = new Player[2];
        }

        //main game loop
        public bool PlayOneGame()
        {
            bool gameOver = false;
            bool playAgain = false;
            int current = 0;

            //set board to "beginning of game" state; all positions are empty
            InitBoard();

            //welcome to tic tac toe
            if (firstSession)
            {
                go.DoIntro();
                go.PromptAnyKey();
                gi.WaitForAnyKey();
            }

            Console.Clear();

            //players choose the letter (X or O) they will play as
            ChooseLetters();
            go.AnnounceLetters(p1, p2);

            //the order in which players make moves is randomly decided (coin flip)
            DecideFirstPlayer();
            go.AnnounceFirstPlayer(players[0]);
            go.PromptAnyKey();
            gi.WaitForAnyKey();

            while (!gameOver)
            {
                Console.Clear();
                //one move
                go.AnnounceTurn(players[current]);
                AdvanceOneMove(current);

                SessionInfo sessionInfo = board.CheckBoardForGameOver();

                if(sessionInfo.endState == GameState.Win) //game ends in a win
                {
                    gameOver = true;
                    FlashBoard(60);
                    go.AnnounceWin(players[current]);
                    board.PrintBoard();
                }
                else if(sessionInfo.endState == GameState.Draw) //game ends in a draw
                {
                    gameOver = true;
                    Console.Clear();
                    go.AnnounceDraw();
                    board.PrintBoard();
                }

                //switch current player
                if (current == 0)
                {
                    current = 1;
                }
                else if (current == 1)
                {
                    current = 0;
                }
            }

            //ask to play again
            go.AskToPlayAgain();
            playAgain = gi.AskYesOrNo();

            if(!playAgain)
            {
                go.SayGoodbye();
                gi.WaitForAnyKey();
            }

            return playAgain;
        }

        //this method needs to be broken up, but oh well
        private void AdvanceOneMove(int currentPlayerIndex)
        {
            bool legal = false;
            char moveIndex; //NOTE: This shit right here is what's going wrong (probably). Char->int conversion is not going how it should, which makes Board.PositionFromIndex return a dummy BoardPosition
            int moveIndexInt;
            BoardPosition movePosition = new BoardPosition(69, PositionState.Empty, new int[2] { 666, 666 });

            board.PrintBoard();
            go.DoInstructions(); 

            while (!legal)
            {
                moveIndex = gi.GetIndex();
                moveIndexInt = (int)Char.GetNumericValue(moveIndex);
                movePosition = board.PositionFromIndex(moveIndexInt);
                legal = board.CheckBoardForLegalMove(movePosition);
                if (legal)
                {
                    movePosition.state = players[currentPlayerIndex].playerInfo.playerState;
                    board.ChangeBoard(movePosition);
                }
            }
        }
        private void FlashBoard(int timesToFlash)
        {

            for(int i = timesToFlash; i > 0; i--)
            {
                Console.WriteLine();
                Console.WriteLine();
                board.PrintBoard();
                Console.Clear();
            }
        }
        private void DecideFirstPlayer()
        {
            Random r = new Random();
            int rr = r.Next(1, 3);

            switch (rr)
            {
                case 1:
                    players[0] = p1;
                    players[1] = p2;
                    break;
                case 2:
                    players[0] = p2;
                    players[1] = p1;
                    break;
                default:
                    break;

            }
        }

        private void InitBoard()
        {
            board = bf.CreateBoard();
        }

        private void ChooseLetters()
        {
            go.DoLetterChoice();

            char xo = gi.GetKey();
            while (xo != 'X' && xo != 'O')
            {
                xo = gi.GetKey();
            }

            p1.PlayerInit(xo, Player.playerName1); //player 1 always gets first choice!

            switch (xo)
            {
                case 'X':
                    p2.PlayerInit('O', Player.playerName2);
                    break;
                case 'O':
                    p2.PlayerInit('X', Player.playerName2);
                    break;
                default:
                    break;
            }
        }
    }
}
