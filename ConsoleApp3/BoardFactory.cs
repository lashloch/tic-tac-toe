﻿using System;


namespace TicTacToeProject {

    class BoardFactory
    {
        private PositionFactory pf;

        public BoardFactory()
        {
            this.pf = new PositionFactory();
        }

        public Board CreateBoard()
        {
            Board b = new Board();
            int[] tempCoords;

            for (int i = 0; i < b.boardSize; i++)
            {
                for(int j = 0; j < b.boardSize; j++ )
                {
                    int tempIndex = ((3 * i + j) + 1);
                    tempCoords = new int[] { i, j };
                    b.positions[i][j] = pf.CreatePosition(PositionState.Empty, tempIndex, tempCoords);
                }
            }

            return b;
         }
    }
}